import java.util.Scanner;

public class ClaseEnum {

    enum Talla{

        MINI("S"), MEDIANO("M"), GRANDE("L"), EXTRAGRANDE("XL");

        private String abrv;

        Talla(String abrv){
            this.abrv = abrv;
        }

        public String getAbrv() {
            return abrv;
        }
    }


//    enum TALLA {MINI, MEDIANO, GRANDE};

    public static void main(String[] args){

        /*
        Objeto Enum, donde se puede almacenar unos tipos
        valores y solo se puede almacenar ese tipo de valores
        y no acepta otros tipos mas
         */

        /*TALLA s = TALLA.MINI;
        TALLA m = TALLA.MEDIANO;
        TALLA l = TALLA.GRANDE;*/

//        System.out.printf("%s\n",s.name());


        Scanner sc = new Scanner(System.in);
        System.out.printf("Escribe una talla, MINI, MEDIANO, GRANDE O EXTRAGRANDE\n");
        String datos = sc.next().toUpperCase();

        Talla talla = Enum.valueOf(Talla.class, datos);

        System.out.printf("La talla ingresada es %s\n", talla.getAbrv());


    }

}

