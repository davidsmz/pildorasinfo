package poo;

public class EmpleadoUso {
    public static void main(String[] args){

        /*
        Polimorfismo, se puede utilizar un objeto de la subclase, siempre
        que el programa espere un objeto de la superclase.
        Es decir un objeto de la superclase puede recibir un objeto de la subclase.
        Un objeto puede comportarse de dediferente forma dependiendo del
        conexto, Las variables objeto son polimorficas.
        */

        Jefe jefeMarketing = new Jefe("Neill", 4000, 2005, 1, 1);
        jefeMarketing.setIncentivo(2000);

        EmpleadoUso obj = new EmpleadoUso();

        Empleado empleado1 = new Empleado("David", 2000, 2010, 9, 6);
        Empleado empleado2 = new Empleado("Caleb", 3000, 2015, 7, 3);
        Empleado empleado3 = new Empleado("Danie", 5000, 2017, 8, 4);
        Empleado empleado4 = new Empleado("Evely");
        // Ejemplo de polimorfismo
        Empleado empleado5 = jefeMarketing;

        Empleado[] empleados = new Empleado[6];
        empleados[0] = empleado1;
        empleados[1] = empleado2;
        empleados[2] = empleado3;
        empleados[3] = empleado4;
        empleados[4] = empleado5;
        // Otro ejemplo de polimorfismo
        // Dentro del objeto Empleado esta entrando un objto Jefe
        // En este caso el empleado[5] se comportaria como objeto empleado
        empleados[5] = new Jefe("Maria", 7000, 2008, 7, 17);

        // Casting de objetos o Refundicion
        Jefe jefeFinanza = (Jefe)empleados[5];
        jefeFinanza.setIncentivo(1000);

        // Vemos como no se puede hacer cast de Empleado a Jefe
        // Esto se debe a la herencias
        // Ya que un Jefe siempre es un empleado en tanto que viceversa no
        // Jefe jefeCompras = (Jefe)empleados[1];

        // Imprimir Datos de empleados
        obj.motrarDatos(empleados);

        // Aumentar el sueldo de empleados en un porcentaje
        // El objeto JefeMarketing se comporta com objeto de clase Empleado
        // Entonces aumenta el sueldo pero como objeto empleado
        // Entonces no aumento el sueldo de la clase Jefe solo Empleado
        obj.subirSueldo(empleados, 10);

        // Imprimir Datos de empleados
        // El jefeMarketing se comporta como objeto de clase Jefe
        // Como vemos el objeto jefeMarketing actua como objeto
        // objeto de clase Empleado y objeto de clase Jefe
        // Este comportamiento se debe al polimorfismo
        obj.motrarDatos(empleados);


        /*
        La Maquita virtual de Java es capaz de detectar a que metodo
        tiene que llamar, como vemos en las clases Jefe y Empleado hay
        metodos getSueldo() , entonces la maquina virtual de Java puede
        realizar ese reconociemiento y eso se llama Enlazado Dinamico,
        Es saber en tiempo de ejecucion a que metodo va a llamar
         */

    }

    void motrarDatos(Empleado[] empleados) {
        for (int i = 0; i < empleados.length; i++) {
            System.out.printf("\n");
            System.out.printf("Id: %d - Nombre: %s - Sueldo: %.2f - fecha: %s",
                    empleados[i].getId(),
                    empleados[i].getNombre(),
                    empleados[i].getSueldo(),
                    empleados[i].getFechaContratacion());
        }
        System.out.printf("\n");
    }

    void subirSueldo(Empleado[] empleados, int porcentaje) {
        // El bucle foreach o for mejorado
        for (Empleado e : empleados) {
            e.setSubirSueldo(porcentaje);
        }
    }

}
