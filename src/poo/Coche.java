package poo;

public class Coche {

    private int ruedas;
    private int ancho;
    private int largo;
    private int motor;
    private int pesoBase;
    private String color;
    private int pesoTotal;
    private int pesoCapote;
    private boolean asientosCuero;
    private boolean climatizador;
    private int precio;

    // Metodo Constructor, es el que da el estado inicial a los objetos
    // El metodo constructor lleva el mismo nombre que la clase

    public Coche(){
        ruedas = 4;
        ancho = 200;      // cm
        largo = 300;      // cm
        motor = 1600;     // cm3
        pesoBase = 500;   // kg
        precio = 15000;   // dolares
    }

    /*
    Metodo getter and setter
    - getter:
      proporcionar el valor de la propiedad
    - setter:
      modificamos el valor de la propiedad
    */

    // geetter ruedas
    public int getRuedas() {
        return ruedas;
    }

    // getter  ancho
    public int getAncho() {
        return ancho;
    }

    // getter  largo
    public int getLargo() {
        return largo;
    }

    // getter motor
    public int getMotor() {
        return motor;
    }

    // getter pesoBase
    public int getPeso() {
        return pesoBase;
    }

    // getter and setter de color

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    // getter and setter de asientosCuero
    public String getAsientosCuero() {
        if (asientosCuero){
            return "si";
        }else {
            return "no";
        }
    }

    public void setAsientosCuero(String asientosCuero) {
        if (asientosCuero.equalsIgnoreCase("si")){
            this.asientosCuero = true;
        }else {
            this.asientosCuero = false;
        }
    }

    // getter and setter de climatizador


    public String getClimatizador() {
        if (climatizador){
            return "si";
        }else {
            return "no";
        }
    }

    public void setClimatizador(String climatizador) {
        if (climatizador.equalsIgnoreCase("si")){
            this.climatizador = true;
        }else {
            this.climatizador = false;
        }
    }

    // getter and setter pesoCapote
    public int getPesoCapote() {
        return pesoCapote;
    }

    public void setPesoCapote(int pesoCapote){
        this.pesoCapote = pesoCapote;
    }

    // getter pesoTotal
    public int getPesoTotal() {
        pesoTotal = pesoBase + pesoCapote;
        if (asientosCuero) {
            pesoTotal = pesoTotal + 20;
        }

        if (climatizador) {
            pesoTotal = pesoTotal + 10;
        }
        return pesoTotal;
    }

    // getter precio
    public int getPrecio() {
        if (asientosCuero) {
            precio += 3000;
        }

        if (climatizador) {
            precio += 4000;
        }

        return precio;
    }

}
