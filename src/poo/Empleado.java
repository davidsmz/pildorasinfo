package poo;

import java.util.Date;
import java.util.GregorianCalendar;

public class Empleado extends Persona {
    // nombre, sueldo, fechadealta (propiedades)
    // subirSueldo (metodo)

    // El constructor por defecto es aquel que sus estados iniciales son 0 o null

    public Empleado(String nombre, double sueldo, int year, int month, int day){
        super(nombre);
        this.sueldo = sueldo;
        // month va de 0 a 11
        GregorianCalendar fecha = new GregorianCalendar(year, month-1, day);
        this.fechaContratacion = fecha.getTime();

        this.id = idSiguiente;
        idSiguiente++;
    }

    // Sobrecarga de Contructores, varis constructores en una misma clase
    // No puede haber otro constructor con el mismo numero de parametros
    // y el mismo orden de los tipos de parametros
    // Contructor con un estado inicial diferente

    public Empleado(String nombre){
        // empleo del this en el contructor
        // para establecer propipedades por defecto
        //this.nombre = nombre;
        this(nombre, 1000, 2017, 1, 1);
    }

    /*
      Tambien existe sobrecarga de metodos donde se cumplen la misma
      condicion que los contructores no pueden tener 2 metodos el mismo
      numero de datos y el mismo orden de tipo de datos como parametros
    */


    // getter nombre
    // Este metodo es heredado de la clase Persona
    /*public String getNombre() {
        return nombre;
    }*/


    // getter sueldo
    // Si utilizamos final en este metodo
    // no se podria crear otro metodo con el mismo nombre
    // en ninguna de las clases sub heredadas
    public double getSueldo() {
        return sueldo;
    }


    // getter fechaContratacion
    public Date getFechaContratacion() {
        return fechaContratacion;
    }


    // setter
    public void setSubirSueldo(double porcentaje){
        sueldo += (sueldo * porcentaje)/100;
    }

    // getter


    public int getId() {
        return id;
    }

    // metodo de la clase abstracta
    @Override
    public String getDescripcion() {
        return "El empleado con id: " + id + " tiene un sueldo de S/" + getSueldo();
    }

    private String nombre;
    private double sueldo;
    private Date fechaContratacion;
    private int id;
    private static int idSiguiente = 1;
}
