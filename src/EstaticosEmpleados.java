public class EstaticosEmpleados {
    public static void main(String[] args){
        Empleados empleado1 = new Empleados("David");
        Empleados empleado2 = new Empleados("Caleb");
        Empleados empleado3 = new Empleados("Evely");

        Empleados[] empleados = new Empleados[3];
        empleados[0] = empleado1;
        empleados[1] = empleado2;
        empleados[2] = empleado3;

        for (Empleados e : empleados) {
            System.out.printf("%s\n", e.getDatos());
        }

        // Metodos estáticos son los que pertenecen a la propia clase
        // Es decir no pertenece a algun objeto instaciado
        System.out.printf("%s\n", Empleados.getIdsiguiente());
    }
}

class Empleados {
    private final String nombre;
    private String seccion;

    // Cuando uno crea una variable, cada vez que hay una instancia de clase
    // Se crea una copia de cada atributo
    // Y cuando se utiliza static en el atributo ya no crea una copia
    // Sino utilizan todas las instancias el mismo atributo
    private int id;
    private static int idSiguiente = 1;

    public Empleados(String nombre){
        this.nombre = nombre;
        this.seccion = "Administracion";
        this.id = idSiguiente;
        idSiguiente++;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    // El metodo estatico solo puede acceder a propiedades estaticas
    // El metodo estatico no puede ser llamado desde un objeto
    // El metodo estatico solo puede ser llamado desde la misma clase
    public static String getIdsiguiente(){
        return "El id siguente es : " + idSiguiente;
    }

    public String getDatos(){
        return "Nombre: " + nombre + " - Seccion: " + seccion + " - id: " + id;
    }
}
